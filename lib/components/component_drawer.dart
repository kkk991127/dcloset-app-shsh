import 'package:dclost_app/pages/codi_list.dart';
import 'package:dclost_app/pages/page_magazine.dart';
import 'package:dclost_app/pages/page_main.dart';
import 'package:dclost_app/pages/page_notice.dart';
import 'package:dclost_app/pages/question/page_question.dart';
import 'package:dclost_app/pages/page_service_introduce.dart';
import 'package:flutter/material.dart';

class ComponentDrawer extends StatelessWidget {
  const ComponentDrawer({super.key});

  @override
  Widget build(BuildContext context) {
    // 드로어는 위에 있으므로 상대적으로 필요성이 떨어지는 메뉴 위주로 구성
    return Drawer(
      child: ListView(
        children: [
          ListTile(
            leading: Icon(Icons.menu_book_rounded),
            iconColor: Colors.grey,
            title: Text('디클로젯 소개'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageServiceIntroduce()));
              },
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.library_books),
            iconColor: Colors.grey,
            title: Text('디클로젯 매거진'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMagazine()));
            },
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.question_answer_rounded),
            iconColor: Colors.grey,
            title: Text('Q&A 게시판'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageQuestion()));
            },
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.redeem),
            iconColor: Colors.grey,
            title: Text('이벤트 게시판'),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMain()));
            },
            trailing: Icon(Icons.navigate_next),
          ),
          ListTile(
            leading: Icon(Icons.info_outline),
            iconColor: Colors.grey,
            title: Text('공지사항 게시판'),
            onTap: () {
              // 공지사항 게시판 만들고 네비게이터 푸시 페이지 변경 필요.
              // 페이지 변경 하였습니다 !!
              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNotice()));
            },
            trailing: Icon(Icons.navigate_next),
          ),
        ],
      ),
    );
  }
}
