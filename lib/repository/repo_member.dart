

import 'package:dclost_app/config/config_api.dart';
import 'package:dclost_app/model/member/common_result.dart';
import 'package:dclost_app/model/member/member_change_info.dart';
import 'package:dclost_app/model/member/member_create_request.dart';
import 'package:dclost_app/model/member/member_detail_result.dart';
import 'package:dio/dio.dart';

class RepoMember{

  //회원 정보 단수 불러오기 (회원 id로)
  Future<MemberDetailResult> getMember(num memberId) async {

    Dio dio = Dio();

    String _baseUrl = '$apiUrl/Member/detail/{id}';

    final response = await dio.get(
      _baseUrl.replaceAll('{id}', memberId.toString()),
        options: Options(
          followRedirects: false,
          validateStatus: (state){
            return state == 200;
          }
        )
    );
    return MemberDetailResult.fromJson(response.data);
  }
  //회원 가입 등록하기
  Future<CommonResult> setMember(MemberCreateRequest request) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUrl/Member/new';

    print(request.toJson());
    final response = await dio.post(
      _baseUrl ,
        data: request.toJson(),//여기서 data는 createRequest
        options: Options(
        followRedirects: false,
        validateStatus: (state){
          return state == 200;
        })
      );

      return CommonResult.fromJson(response.data);//여기서 data는 승인된 값을 반환해서 받는 crateResult
  }
  // 회원 정보 수정하기


  Future<CommonResult> putMember(MemberChangeInfo request,num id) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUrl/Member/new';

    print(request.toJson());
    final response = await dio.post(
        _baseUrl,
        data: request.toJson(), //여기서 data는 createRequest
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            })
    );

    return CommonResult.fromJson(response.data);
  }

}