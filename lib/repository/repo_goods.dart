



import 'package:dclost_app/config/config_api.dart';
import 'package:dclost_app/model/goods/good_list_result.dart';
import 'package:dclost_app/model/goods/goods_detail_result.dart';

import 'package:dio/dio.dart';

class RepoGoods {

  Future<GoodsListResult> getGoodsList() async {

    Dio dio = Dio();

    String _baseUrl = '$apiUrl/goods/all'; //엔드포인트

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return GoodsListResult.fromJson(response.data);
  }




  //상품 리스트 전체보기
  
  //
  // Future<GoodsListResult> getGoodsList() async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUrl/goods/all'; //엔드포인트
  //
  //   Response response = await dio.get(
  //       _baseUrl,
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (status) {
  //             return status == 200;
  //           }
  //       )
  //   );
  //
  //   List<dynamic> resList = response.data;
  //   print('responseMap: $resList');
  //
  //   List<GoodsItem> list = resList.map((item) => GoodsItem.fromJson(item))
  //       .toList();
  //   print('goodsItem list: $list');
  //
  //   return list;
  // }

  //상품 한개 불러오기
  Future<GoodsDetailResult> getGoods(num goodsId) async {

    Dio dio = Dio();

    String _baseUrl = '$apiUrl/goods/detail/goodsId/{goodsId}';

    final response = await dio.get(
      _baseUrl.replaceAll('{goodsId}', goodsId.toString()),
          options: Options(
        followRedirects: false,
        validateStatus: (state){
          return state == 200;
        }
      )
    );

    return GoodsDetailResult.fromJson(response.data);

  }
}

