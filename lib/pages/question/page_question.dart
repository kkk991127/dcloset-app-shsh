import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/components/component_question_list.dart';
import 'package:dclost_app/model/question/question_form.dart';
import 'package:dclost_app/pages/question/page_question_detail.dart';
import 'package:flutter/material.dart';

class PageQuestion extends StatefulWidget {
  const PageQuestion({super.key});

  @override
  State<PageQuestion> createState() => _PageQuestionState();
}
//
class _PageQuestionState extends State<PageQuestion> {
  // 임의 리스트, 백엔드 연동 시 setState 로 리스트 변경 메서드 발동 필요
  // Q&A 게시판 복수 R 리스트 페이지

  List<QuestionForm> _list = [
    QuestionForm(1, DateTime.utc(2024, 01, 20), 'afeew1', '배송 문의 드려요', '어제 시켰는데 언제 받을 수 있을까요??'),
    QuestionForm(2, DateTime.utc(2024, 01, 24), 'karina42','택배가 안와요', '시킨지 3일 넘었는데 도대체 언제 도착해요 ㅡㅡ'),
    QuestionForm(3, DateTime.utc(2024, 01, 31), 'gagang0976','이거 언제 재입고 되나요?', '이거 너무 예쁜데 계속 품절이네요 ㅠㅠ'),
    QuestionForm(4, DateTime.utc(2024, 02, 05), 'kkk1029', '방금 주문했는데요..', '처음 시켜봐서 그런데 희망 수령날짜에 도착할 수 있을까요?'),
  ];

  //글쓰기 버튼 만들어야함


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentQuestionList(
              questionForm: _list[idx],
              callback: (){
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => PageQuestionDetail(questionForm: _list[idx], )
                )
              );
           },
          );
        },
      ),


    );

  }
}

