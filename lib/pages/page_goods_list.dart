import 'package:dclost_app/model/goods/good_list_result.dart';
import 'package:dclost_app/repository/repo_goods.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../model/goods/goods_item.dart';

class PageGoodsList extends StatefulWidget {
  const PageGoodsList({super.key});

  @override
  State<PageGoodsList> createState() => _PageGoodsListState();

}


class _PageGoodsListState extends State<PageGoodsList> {

  List<GoodsItem> _list = [];

  /*Future<void> _loadList() async {
    await RepoGoods().getGoodsList()
        .then((res) => {
          setState(() {_list = res.list;})})
=======
        .catchError(err) => {debugPrint(err)})
  }*/

  Future<void> _loadList() async {

    await RepoGoods().getGoodsList()
        .then((res) => {
          setState(() {
            if ( res.code == 0 ){
              _list = res.list;
            } else {
              Fluttertoast.showToast(msg: res.msg);
            }
      })
    });
  }

  @override
  void initState(){
    super.initState();

    // 상품목록조회
    _loadList();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('코디 리스트'),
        centerTitle: true,
      ),
      body: GridView.count(
        crossAxisCount: 3, // 1개 행에 항목 3개씩
        mainAxisSpacing: 5,
        crossAxisSpacing: 5,
        childAspectRatio: 1/2,
        children: _list.map((item) {

          // @TODO logoicon 대신 파일 없을 때 조회할 이미지 url 넣기
          String productMainImage = item.productMainImage ?? "logoicon.png";

          return Card(
            elevation: 2,
            child: Column(
              children: <Widget>[
                Expanded(child: Image.network(productMainImage,width: 200, height: 200,),flex: 5,
                ),

              ],
            ),
          );
        }).toList(),
        /*children: List.generate(
          4, // 항목 개수
              (int index) {
            return Card(
              elevation: 2,
              child: Column(
                children: <Widget>[
                  Expanded(child: Image.asset('assets/catalog1.png',width: 200, height: 200,),flex: 5,
                  ),
                  Expanded(child: ListTile(
                   title: Text('꾸안꾸 코디세트'),
                    subtitle: Text('25,000원'),
                    ),
                  ),
                ],
              ),
            );
          },
        ),*/
      ),
    );
  }
}

