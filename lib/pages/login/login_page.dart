import 'package:dclost_app/pages/page_main.dart';
import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
                style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),'Dcloset'),
            TextField(
              controller: _usernameController,
              decoration: InputDecoration(
                labelText: 'Username',
              ),
            ),
            SizedBox(height: 8.0),
            TextField(
              controller: _passwordController,
              decoration: InputDecoration(
                labelText: 'Password',
              ),
              obscureText: true,
            ),
            SizedBox(height: 40),
            ElevatedButton(
              child: Text('Log in'),
              onPressed: () {
                // Implement login functionality here
                Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMain()));
                String username = _usernameController.text;
                String password = _passwordController.text;
                print('Username: $username\nPassword: $password');
              },
              style: ElevatedButton.styleFrom(
                shadowColor: Colors.grey,
                elevation: 10.0,
                backgroundColor: Colors.black,
                foregroundColor: Colors.white,

              ),
            ),
          ],
        ),
      ),
    );
  }
}