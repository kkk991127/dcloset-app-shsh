import 'package:dclost_app/model/notice/notice_response.dart';
import 'package:dclost_app/pages/page_notice.dart';
import 'package:flutter/material.dart';

class PageNoticeDetail extends StatefulWidget {
  const PageNoticeDetail({super.key, required this.noticeResponse});

  final NoticeResponse noticeResponse;

  @override
  State<PageNoticeDetail> createState() => _PageNoticeDetailState();
}
class _PageNoticeDetailState extends State<PageNoticeDetail> {

  /// 공지사항 디테일 페이지 입니다.

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
            widget.noticeResponse.noticeTitle, style: TextStyle(fontSize: 18),),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(3),
             decoration: BoxDecoration(
               borderRadius: BorderRadius.circular(0),
               border: Border.all(
                 width: 1,
                 color: Colors.black12,
               ),
             ),
                child: Text('작성자 : 관리자'),),
            Container(
                padding: EdgeInsets.all(20),
                height: 300,
                width: 300,
                decoration: BoxDecoration(border: Border.all(color: Colors.grey,width: 1)),
                child: Image.asset(widget.noticeResponse.noticeImage)
            ),
            Container(
             margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                border: Border.all(
                  width: 1,
                  color: Colors.white70
                )
              ),
              child: Text(
                '' + widget.noticeResponse.noticeContent,
              style: TextStyle(
                  fontSize: 14
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.all(10),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  //엘리베이터 버튼 스타일
                  minimumSize: Size(150, 50), // 버튼 사이즈 조절
                  backgroundColor: Colors.black,
                  foregroundColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30)),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageNotice()));
                }, child: Text('목록'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}