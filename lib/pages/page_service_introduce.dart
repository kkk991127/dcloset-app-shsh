import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/pages/page_goods_list.dart';
import 'package:dclost_app/pages/login/page_membership.dart';
import 'package:flutter/material.dart';

class PageServiceIntroduce extends StatefulWidget {
  const PageServiceIntroduce({super.key});

  @override
  State<PageServiceIntroduce> createState() => _PageServiceIntroduceState();
}

class _PageServiceIntroduceState extends State<PageServiceIntroduce> {
  // 디클로젯 서비스 소개 페이지

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: SingleChildScrollView(
        child: Container(
          decoration: BoxDecoration(
            color: Colors.deepPurple.shade50,
          ),
          margin: EdgeInsets.fromLTRB(0, 0, 0, 0),
          alignment: Alignment.center,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                child: Text(' " 옷을 아무리 사도 마음에 드는 옷이 없는 당신에게,',
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ),
              Text(' 디클로젯이 찾아갑니다. " ',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 20),
                child: Image.asset(
                    'assets/introduceimg.jpg',
                    fit: BoxFit.fill,
                ),
              ),
              Container(
                padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                child: Column(
                  children: [
                    Text(
                      '전 세계적으로 1년에 만들어지는 옷 1,000억벌 중 73% 는 팔리지 않고 소각되거나 매립된다고 합니다.',
                      style: TextStyle(
                          fontSize: 16,
                      ),
                    ),
                    Text(
                      '디클로젯은 이러한 폐의류를 재활용 할 수 있는 방법이 없을까 고심했습니다.',
                      style: TextStyle(
                          fontSize: 16
                      ),
                    ),
                    Text(
                      '이에 마침내 탄생한 디클로젯의 의류 렌탈 서비스를 소개합니다.',
                      style: TextStyle(
                          fontSize: 16
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                child: Image.asset('assets/Dclosetservice.png'),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                alignment: Alignment.center,
                child: Column(
                  children: [
                    Column(
                      children: [
                        Container(

                          padding: EdgeInsets.all(10),
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMembership()));
                            },
                            child: Text('멤버십 구독 신청'),
                            style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                              minimumSize: Size(100, 50),
                              backgroundColor: Colors.black,
                              foregroundColor: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          child: ElevatedButton(
                            onPressed: () {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageGoodsList()));
                            },
                            child: Text('카탈로그 보러 가기'),
                            style: ElevatedButton.styleFrom( //엘리베이터 버튼 스타일
                              minimumSize: Size(100, 50),
                              backgroundColor: Colors.black,
                              foregroundColor: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
