import 'package:dclost_app/components/component_appbar.dart';
import 'package:dclost_app/components/component_drawer.dart';
import 'package:dclost_app/components/component_mypage_item.dart';
import 'package:dclost_app/model/mypage/mypage_item.dart';
import 'package:dclost_app/pages/mypage/page_member_correct.dart';
import 'package:flutter/material.dart';

class PageMyPage extends StatefulWidget {
  const PageMyPage({super.key});

  //마이페이지
  @override
  State<PageMyPage> createState() => _PageMyPageState();
}

class _PageMyPageState extends State<PageMyPage> {
  List<MyPageItem> _list =
  [
   MyPageItem(1, '회원 정보 수정'),
    MyPageItem(2, '문의 내역 조회'),
    MyPageItem(1, '나의 회원 등급 조회'),

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbar(),
      drawer: ComponentDrawer(),
      body: ListView.builder(
        itemCount: _list.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentMyPageItem(myPageItem: _list[idx], callback: () {Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMemberCorrect()));});
        },
      ),
    );
  }
}
