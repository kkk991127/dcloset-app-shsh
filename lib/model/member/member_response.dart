
import 'package:intl/intl.dart';

class MemberResponse {

   num id;
   String username;
   String memberName;
   String birthDay;
   String phoneNumber;
   String memberAddress;
   String memberDetailedAddress;
   num postCode;
   String subscriptDate;
   bool nYPersonalInfo;
   bool nYMarketing;
   String memberGrade;
   num remainTime;
   String payWay;
   String payInfo;
   String payDay;

   MemberResponse(this.id,this.username,this.memberName,this.birthDay,
       this.phoneNumber,this.memberAddress,this.memberDetailedAddress,
       this.postCode,this.subscriptDate,this.nYPersonalInfo,this.nYMarketing,
       this.memberGrade,this.remainTime,this.payWay,this.payInfo,this.payDay);

   factory MemberResponse.fromJson(Map<String,dynamic>json){

      DateTime subscriptDate = DateTime.parse(json["subscriptDate"]);
      String subscriptDateString = DateFormat('yyyy/MM/dd hh:mm').format(subscriptDate);

     return MemberResponse(
         json['id'],
         json['username'],
         json['memberName'],
         json['birthDay'],
         json['phoneNumber'],
         json['memberAddress'],
         json['memberDetailedAddress'],
         json['postCode'],
         subscriptDateString,
         json['NYPersonalInfo'],
         json['NYMarketing'],
         json['memberGrade'],
         json['remainTime'],
         json['payWay'],
         json['payInfo'],
         json['payDay']
     );

   }

}