
class MemberChangeInfo{

   String memberName;
   String password;
   String passwordRe;
   String phoneNumber;
   String memberAddress;
   String memberDetailedAddress;
   num postCode;
   bool nYMarketing;
   num remainTime;// 이건 가져오기만하고 회원이 수정할수없게
   String payWay;
   String payInfo;
   String payDay;
   String memberGrade;

   MemberChangeInfo(
       this.memberName,
       this.password,
       this.passwordRe,
       this.phoneNumber,
       this.memberAddress,
       this.memberDetailedAddress,
       this.postCode,
       this.nYMarketing,
       this.remainTime,
       this.payWay,
       this.payDay,
       this.payInfo,
       this.memberGrade
       );

   factory MemberChangeInfo.fromJson(Map<String,dynamic>json){
     return MemberChangeInfo(
         json['memberName'],
         json['password'],
         json['passwordRe'],
         json['phoneNumber'],
         json['memberAddress'],
         json['memberDetailedAddress'],
         json['postCode'],
         json['nYMarketing'],
         json['remainTime'],
         json['payWay'],
         json['payDay'],
         json['payInfo'],
         json['memberGrade']);
   }

   Map<String,dynamic>toJson() => {
   'memberName':memberName,
   'password':password,
   'passwordRe':passwordRe,
   'phoneNumber':phoneNumber,
   'memberAddress':memberAddress,
   'memberDetailedAddress':memberDetailedAddress,
   'postCode':postCode,
   'nYMarketing':nYMarketing,
   'remainTime':remainTime,
   'payWay':payWay,
   'payDay':payDay,
   'payInfo':payInfo,
   'memberGrade':memberGrade

   };




}