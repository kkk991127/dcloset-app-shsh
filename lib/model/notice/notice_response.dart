class NoticeResponse{
   num id;
   dynamic member;
   String noticeCreateDate;
   String noticeTitle;
   String noticeContent;
   String noticeImage;

   NoticeResponse(
       this.id,
       this.member,
       this.noticeCreateDate,
       this.noticeTitle,
       this.noticeContent,
       this.noticeImage
       );

   factory NoticeResponse.fromJson(Map<String,dynamic>json){

     return NoticeResponse(
         json['id'],
         json['member'],
         json['noticeCreateDate'],
         json['noticeTitle'],
         json['noticeContent'],
         json['noticeImage']
     );

   }






}